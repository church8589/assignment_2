import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class DemoWritingToFiles {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		//Replacing previous code with method and calling it
		saveToFile("out.txt", "Hello Everyone", false);
		
		
		/**
		File file = new File("out.txt");			//created a File object called file
		FileWriter fw= new FileWriter(file, true);	//created a FileWriter object called fw
		PrintWriter pw = new PrintWriter(fw);		//created a PrintWriter object called pw
		
		
		//Adding some lines to the document
		pw.println("Line 1");
		pw.println("Line 2");
		pw.println("Line 3");
		
		//closing PrintWriter object
		pw.close();
		*/
	}
	
	
	public static void saveToFile(String file, String text, boolean append) {
		try {
			File f = new File(file);
			FileWriter fw = new FileWriter(f);
			PrintWriter pw= new PrintWriter(fw);
			
			pw.println(text);
			pw.close();
		}
		catch(IOException e) {
			System.out.println("Error: saveToFile");
		}
	}
}
