import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class DemoWritingToFiles1 {

	public static void main(String[] args) throws IOException {
		
		Car[] cars = new Car[5];
		
		cars[0] = new Car(2018, "Ford", "Mustang", 25000, 5.8d);
		cars[1] = new Car(1905, "Ford", "Model-T", 82209, 1.8d);
		cars[2] = new Car(2016, "GMC", "Silverado", 10000, 7.5d);
		cars[3] = new Car(2018, "Ford", "Focus", 99000, 1.2d);
		cars[4] = new Car(2018, "Toyota", "Prius", 160000, 2.1d);
		
		for (int x = 0; x < cars.length; x++) {
			String someText = Integer.toString(cars[x].getYear()) + "|" + cars[x].getMake() + "|" + cars[x].getModel() + "|" + 
		cars[x].getOdometer() + "|" + Double.toString(cars[x].getEngineLiters());
			saveToFile("carsList.txt", someText, true);
		}
		
		//now read the results and print them to the console
		
		ArrayList<Car> carsReadFromFile = new ArrayList<>();
		carsReadFromFile = readCarsFromFile("carsList.txt");
		
		System.out.println(carsReadFromFile);
		
		/**
		for(int i = 0; i < carsFromFile.size(); i++) {
			System.out.println(carsFromFile.get(i).getMake() + carsFromFile.get(i).getModel() + carsFromFile.get(i).getYear() +
					carsFromFile.get(i).getOdometer() + carsFromFile.get(i).getEngineLiters());
		}
		*/
	}

	public static void saveToFile(String file, String text, boolean append) {
		try {
			File f = new File(file);
			FileWriter fw = new FileWriter(f, append);
			PrintWriter pw= new PrintWriter(fw);
			
			pw.println(text);
			pw.close();
		}
		catch(IOException e) {
			System.out.println("Error: saveToFile");
		}
	}
	
	public static ArrayList<Car> readCarsFromFile(String fileName) throws FileNotFoundException{
		
		File file = new File(fileName);
		Scanner s = new Scanner(file);
		
		ArrayList<Car> carList = new ArrayList<>();
		
		while (s.hasNextLine()) {
			String line = s.nextLine();
			
			String [] items = line.split("\\|");
			
			System.out.println(items[0]);
			System.out.println(items[1]);
			System.out.println(items[2]);
			System.out.println(items[3]);
			System.out.println(items[4]);
			
			int year = Integer.valueOf(items[0]);
			String make = items[1];
			String model = items[2];
			int odometer = Integer.valueOf(items[3]);
			double engineSize = Double.valueOf(items[4]);
			
			Car newCar = new Car(year, make, model, odometer, engineSize);
			carList.add(newCar);
			
			
		}
		
		return carList;
	}
	
	public static ArrayList<Car> readFromFile(String fileName) throws FileNotFoundException{
		
		File file = new File(fileName);
		Scanner s = new Scanner(file);
		
		ArrayList<Car> cars = new ArrayList<>();
		while(s.hasNextLine()) {
			String line = s.nextLine();
			String[] items = new String[5];
			items = line.split("\\|");
			int year = Integer.valueOf(items[0]);
			String make = items[1];
			String model = items[2];
			int odometer = Integer.valueOf(items[3]);
			double engineSize = Double.valueOf(items[4]);
			
			Car c = new Car(year, make, model, odometer, engineSize);
			cars.add(c);
		}
		return cars;
	}
}
