package week4dq1;

public abstract class Playable {

	public abstract void play();
    public abstract void prepareToPlay();
}
