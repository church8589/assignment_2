package week4dq1;

public class videoGame extends Playable {

	public boolean cartridgeInstalled;
    public int controllersConnected;
    public boolean consoleOn;
    boolean playing;
    
	@Override
	public void play(){
        if(consoleOn){
            playing = true;
        }
    }

	@Override
	public void prepareToPlay(){
        if(cartridgeInstalled == true && controllersConnected > 0){
            consoleOn = true;
        }
    }

}
