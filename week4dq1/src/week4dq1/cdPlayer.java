package week4dq1;

public class cdPlayer extends Playable {
	public boolean readyToPlay;
	public boolean play;
	public int cdsInExchanger;

	@Override
	public void play(){
		if(readyToPlay == true){
			play = true;
		}
	}

	@Override
	public void prepareToPlay(){
		if(cdsInExchanger > 0){
			readyToPlay = true;
		}

	}

}
