package base;
/**
 * 
 * @author Andrew Wilkerson
 * CST-135
 * This is my own original work
 *
 */
public interface ShapeInterface {

	int calculateArea();
}
