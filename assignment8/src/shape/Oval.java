package shape;

import base.ShapeBase;
/**
 * 
 * @author Andrew Wilkerson
 * CST-135
 * This is my own original work
 *
 */
public class Oval extends ShapeBase {
	/**
	 * Added requred fields to get area for Oval
	 */
	private int majorAxis;
	private int minorAxis;

	/**
	 * Only calling super constructor for name
	 * @param name
	 * @param majorAxis Needed to calculate area
	 * @param minorAxis Needed to calculate area
	 */
	public Oval(String name, int majorAxis, int minorAxis) {
		super(name, 0, 0);
		this.majorAxis = majorAxis;
		this.minorAxis = minorAxis;
	}
	
	/**
	 * Specific implementation of area for ovals
	 */
	@Override
	public int calculateArea() {
		int area = (int)(Math.PI * (this.majorAxis/2) * (this.minorAxis/2));
		return area;
	}

}
