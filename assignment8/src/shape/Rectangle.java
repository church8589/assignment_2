package shape;

import base.ShapeBase;
/**
 * 
 * @author Andrew Wilkerson
 * CST-135
 * This is my own original work
 *
 */
public class Rectangle extends ShapeBase {

	/**
	 * Calling super constructor
	 * @param name
	 * @param width Needed for calculating area
	 * @param height Needed for calculating area
	 */
	public Rectangle(String name, int width, int height) {
		super(name, width, height);
	}
	
	/**
	 * Specific implementation of area of shapes with four straight sides
	 */
	@Override
	public int calculateArea(){
		return width * height;
	}

}
