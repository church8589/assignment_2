package shape;

import base.ShapeBase;
/**
 * 
 * @author Andrew Wilkerson
 * CST-135
 * This is my own original work
 *
 */
public class Circle extends ShapeBase {
	/**
	 * This is to add radius needed for area of circle
	 */
	private int radius;

	/**
	 * Only calling super constructor for name
	 * @param name
	 * @param radius used for calculating area
	 */
	public Circle(String name, int radius) {
		super(name, 0, 0);
		this.radius = radius;
		// TODO Auto-generated constructor stub
	}
	/**
	 * Specific implementation of area for circles
	 */
	@Override
	public int calculateArea() {
		int area = (int) (Math.pow(this.radius, 2) * Math.PI);
		return area;
	}
}
