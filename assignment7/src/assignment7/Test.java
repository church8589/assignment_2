package assignment7;

import java.util.Arrays;

public class Test {

	public static void main(String[] args) {
		//Create new Person objects
		Person person1 = new Person("Andrew", "Wilkerson", 34);
		Person person2 = new Person("Jessica", "Wilkerson", 27);
		Person person3 = new Person(person1);
		
		//Test Object Equality
		if(person1 == person2) {
			System.out.println("These persons are indetical using ==");
		}
		else {
			System.out.println("These persons are not identical using ==");
		}
		
		if(person1.equals(person2)) {
			System.out.println("These persons are identical using equals()");
		}
		else {
			System.out.println("These persons are not identical using equals()");
		}
		
		if(person1.equals(person3)) {
			System.out.println("These persons are identical using equals()");
		}
		else {
			System.out.println("These persons are not identical using equals()");
		}
		
		System.out.println(person1.toString());
		System.out.println(person2.toString());
		
		//make a person walk and run
		person1.walk();
		person1.run();
		System.out.println("Person 1 is running: " + person1.isRunning());
		person1.walk();
		System.out.println("Person 1 is running: " + person1.isRunning());
		
		//Create 10 people and compare them so they are sorted by last name
		Person[] persons = new Person[10];
		persons[0] = new Person("Nolan", "Wilkerson", 1);
		persons[1] = new Person("Justine", "Smith", 10);
		persons[2] = new Person("Ashley", "Hemmingway", 15);
		persons[3] = new Person("Ashley", "AppleWood", 98);
		persons[4] = new Person("Stephanie", "Hemmingway", 76);
		persons[5] = new Person("Jessica", "Davis", 65);
		persons[6] = new Person("Ashley", "Merritt", 88);
		persons[7] = new Person("Caitlin", "Stockset", 25);
		persons[8] = new Person("Chad", "Davis", 18);
		persons[9] = new Person("CJ", "Wallace", 3);
		Arrays.sort(persons);
		for(Person person : persons) {
			System.out.println(person.toString());
		}

	}

}
