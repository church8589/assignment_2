package assignment7;
/**
 * 
 * @author Andrew Wilkerson
 * CST-135
 * This is my own original work.
 *
 */
public interface PersonInterface {
	
	public void walk();
	public void run();
	public boolean isRunning();


}
