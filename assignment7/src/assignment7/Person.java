package assignment7;
/**
 * 
 * @author Andrew Wilkerson
 * CST-135
 * This is my own original work.
 */
public class Person implements PersonInterface, Comparable<Person>{
	private String firstName;
	private String lastName;
	private boolean running;
	private Integer age;
	
	public Person(String firstName, String lastName, int age) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
	}
	
	public Person(Person person) {
		this.firstName = person.getFirstName();
		this.lastName = person.getLastName();
	}
	
	@Override
	public boolean equals(Object other) {
		if(other == this) {
			System.out.println("I am in the other == this");
			return true;
		}
		if(other == null) {
			System.out.println("I am in the other == null");
			return false;
		}
		if(getClass() != other.getClass()) {
			System.out.println("I am here in getClass() != other.getClass()");
			return false;
		}
		
		Person person = (Person)other;
		return(this.firstName == person.firstName && this.lastName == person.lastName);
	}
	
	/**
	 * overriding the toString() to print out the first and last name, with the age of person
	 */
	@Override
	public String toString() {
		return "My class is " + getClass() + " " + this.firstName + " " + this.lastName + " " + "Age: " + age;
	}
	
	
	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	/**
	 * implementing the walk() from PersonInterface interface
	 */
	@Override
	public void walk() {
		System.out.println("I am walking");
		running = false;
		
	}

	/**
	 * implementing the run() from PersonInterface interface
	 */
	@Override
	public void run() {
		System.out.println("I am running");
		running = true;

	}

	/**
	 * implementing the isRunning() from PersonInterface interface
	 */
	@Override
	public boolean isRunning() {
		return running;
	}
	
	/**
	 * changing the compareTo() to look at the age of the person
	 */
	@Override
	public int compareTo(Person p) {
		
		int value = this.age.compareTo(p.age);
		return value;
		/*
		int value = this.lastName.compareTo(p.lastName);
		if(value == 0) {
			return this.firstName.compareTo(p.firstName);
		}
		else {
			return value;
		}
		*/
	}

	

}
