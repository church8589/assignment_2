package week4dq2;

public class Crickets extends Bug {	
	private int numberOfWings;
	public Crickets(int legs){
		super(legs);
		this.numberOfWings = 2;
	}
	@Override
	public void eats() {
		System.out.println("Crickets eat leaves.");
	}
}
