package week4dq2;

public abstract class Bug {
	private int legs;
	public Bug(int legs){
		this.legs = legs;
	}	
	public void eats() {
		System.out.println("Bugs eat plants.");
	}	
	public void eatenBy() {
		System.out.println("Bugs are eaten by Animals.");
	}	
}
