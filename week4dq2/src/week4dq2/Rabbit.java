package week4dq2;

public class Rabbit implements Animal {
	public int feet;
	public Rabbit(){
		this.feet = 2;
	}
	@Override
	public void eats() {
		System.out.println("Rabbits eat carrots.");
	}
	@Override
	public void eatenBy() {
		System.out.println("Rabbits are eaten by Tigers.");
	}
}
