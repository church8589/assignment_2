package collections;

import java.util.Iterator;
import java.util.Stack;

public class playStack {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Create a stack of Integers and add 5
		Stack<Integer> intStack = new Stack<>();
		intStack.add(1);
		intStack.add(2);
		intStack.add(3);
		intStack.add(4);
		intStack.add(5);
		
		//Create a stack of String and add 5
		Stack<String> strStack = new Stack<>();
		strStack.add("Andrew");
		strStack.add("Wilkerson");
		strStack.add("the");
		strStack.add("absolute");
		strStack.add("best");
		
		//Print the size and head of each queue
		System.out.printf("The size of intStack is: %d, and the 2nd element is %d\n", intStack.size(), intStack.elementAt(1));
		System.out.printf("The size of strStack is: %d, and the 2nd element is %s\n", strStack.size(), strStack.elementAt(1));

		//Print intStack to console with toString()
		System.out.println(intStack.toString());
		
		//Print strStack to console with while loop
		Iterator<String> strIter = strStack.iterator();
		while(strIter.hasNext()) {
			System.out.println(strIter.next());
		}
	}

}
