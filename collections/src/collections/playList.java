package collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class playList {
	
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Create a list of Integers and add 5 elements
		List<Integer> intList = new ArrayList<>();
		intList.add(Integer.valueOf(10));
		intList.add(Integer.valueOf(5));
		intList.add(Integer.valueOf(6));
		intList.add(Integer.valueOf(90));
		intList.add(Integer.valueOf(3));
		
		//Create list of Stings and add 5 elements
		List<String> strList = new ArrayList<>();
		strList.add("Andrew");
		strList.add("Wilkerson");
		strList.add("Hello");
		strList.add("Where");
		strList.add("Clemson");
		
		//Print out first element of each List using get()
		System.out.printf("Intger Value: %d\n", intList.get(0));
		System.out.printf("String Value: %s\n", strList.get(0));
		
		//Loop through the Integer List with For Loop
		for(Integer data : intList) {
			System.out.printf("Integer Value: %d\n", data);
		}
		
		//Loop through the String List with while loop
		Iterator<String> strIterator = strList.iterator();
		while(strIterator.hasNext()) {
			System.out.printf("String Value: %s\n", strIterator.next());
		}
		

	}

}
