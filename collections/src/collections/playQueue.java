package collections;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class playQueue {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Create Queue of Integers and add 5
		Queue<Integer> intQ = new LinkedList();
		intQ.add(1);
		intQ.add(2);
		intQ.add(3);
		intQ.add(4);
		intQ.add(5);
		
		//Create Queue of Strings and add 5
		Queue<String> strQ = new LinkedList<>();
		strQ.add("Andrew");
		strQ.add("Wilkerson");
		strQ.add("the");
		strQ.add("absolute");
		strQ.add("best");
		
		//Print the size and head of each queue
		System.out.printf("The size of intQ is: %d, and the head is %d\n", intQ.size(), intQ.peek());
		System.out.printf("The size of strQ is: %d, and the head is %s\n", strQ.size(), strQ.peek());

		//Print the intQ with toString()
		System.out.println(intQ.toString());
		
		//Print the strQ with While loop
		Iterator<String> itr = strQ.iterator();
		while(itr.hasNext()) {
			System.out.println(itr.next());
		}
	}

}
