package collections;

import java.util.HashMap;
import java.util.Map;

public class playMap {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Create a Map of Integers 
		Map<Integer, Integer> intMap = new HashMap<Integer, Integer>();
		intMap.put(1, 1);
		intMap.put(2, 2);
		intMap.put(3, 3);
		intMap.put(4, 4);
		intMap.put(5, 5);
		
		//Create map of Strings
		Map<String, String> strMap = new HashMap<>();
		strMap.put("First Name", "Andrew");
		strMap.put("Last Name", "Wilkerson");
		strMap.put("BirthPlance", "Naboo");
		strMap.put("Hobby", "Quidditch");
		strMap.put("Pet", "Gremlin");
		
		//Print out the size and if the Map is empty
		System.out.printf("Integer Map size is: %d and is empty %b\n", intMap.size(), intMap.isEmpty());
		System.out.printf("String Map size is: %d and is empty %b\n", strMap.size(), strMap.isEmpty());
		
		//Use for loop to print out String Map
		for(Map.Entry<String, String> map : strMap.entrySet()) {
			System.out.printf("Key: %s Value: %s\n", map.getKey(), map.getValue());
		}
		
		//Clear the Maps
		intMap.clear();
		strMap.clear();
		
	}

}
