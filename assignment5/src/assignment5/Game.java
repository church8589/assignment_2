package assignment5;
/**
 * 
 * @author Andrew Wilkerson
 *
 */
public class Game {
	/**
	 * Used to implement WeaponInterface and demonstrate its uses in polymorphism
	 * @param args
	 */

	public static void main(String[] args) {
		
		WeaponInterface[] weapons = new WeaponInterface[2];
		
		weapons[0] = new Bomb();
		weapons[1] = new Gun();
		
		for(WeaponInterface weapon : weapons) {
			fireWeapon(weapon);
		}
		
	}

	private static void fireWeapon(WeaponInterface weapon) {
		weapon.activate(true);
		weapon.fireWeapon(10);
	}
}