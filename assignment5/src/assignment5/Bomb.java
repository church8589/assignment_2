package assignment5;
/**
 * 
 * @author Andrew Wilkerson
 *
 */
public class Bomb implements WeaponInterface {

	/**
	 * Fires a weapon with a int @power
	 */
	public void fireWeapon(int power) {
		System.out.printf("In Bomb.fireWeapon() with a power of %d\n", power);
	}
	
	/**
	 * Fires a weapon with no power
	 */
	public void fireWeapon() {
		System.out.println("In Bomb.fireWeapon() with no power.");
	}

	/**
	 * Activates the weapon with boolean @enable
	 */
	public void activate(boolean enable) {
		System.out.printf("In the Bomb.activate() with an enable of %b\n", enable);
		
	}

}
