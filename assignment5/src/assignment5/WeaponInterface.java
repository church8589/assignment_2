package assignment5;
/**
 * 
 * @author Andrew Wilkerson
 * 
 * @apiNote
 * This is used to create weapons and default behavior
 */
public interface WeaponInterface {
	
	public void fireWeapon();
	public void fireWeapon(int power);
	public void activate(boolean enable);

}
