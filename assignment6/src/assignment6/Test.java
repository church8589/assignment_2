package assignment6;

public class Test {

	public static void main(String[] args) {
		//Create new Person objects
		Person person1 = new Person("Andrew", "Wilkerson");
		Person person2 = new Person("Jessica", "Wilkerson");
		Person person3 = new Person(person1);
		
		//Test Object Equality
		if(person1 == person2) {
			System.out.println("These persons are indetical using ==");
		}
		else {
			System.out.println("These persons are not identical using ==");
		}
		
		if(person1.equals(person2)) {
			System.out.println("These persons are identical using equals()");
		}
		else {
			System.out.println("These persons are not identical using equals()");
		}
		
		if(person1.equals(person3)) {
			System.out.println("These persons are identical using equals()");
		}
		else {
			System.out.println("These persons are not identical using equals()");
		}
		
		System.out.println(person1.toString());
		System.out.println(person2.toString());
			

	}

}
