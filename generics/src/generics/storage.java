package generics;

public class storage<T> {
	
	private T s = null;
	
	public storage(T s) {
		this.s = s;
	}
	
	public T getData() {
		return this.s;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		storage<String> storage1 = new storage<>("Clemson");
		System.out.println("This is the data: " + storage1.getData());
		storage<Integer> storage2 = new storage<>(1);
		System.out.println("Clemson should be number " + storage2.getData());

	}

}
