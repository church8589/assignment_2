package generics;

public class myArray {
	/**
	 * Testing out linux manjaro
	 * @param <E>
	 * @param inputArray
	 */
	
	public <E> void printArray(E[] inputArray) {
		for(E element : inputArray) {
			System.out.printf("%s\n", element);
		}
	}

	public static void main(String[] args) {
		// Create Arrays of Integer, double and Character
		Integer[] intArray = {1,2,3,4,5,};
		Double[] doubleArray = {1.1,2.2,3.3,4.4};
		Character[] charArray = {'C', 'L','E', 'M', 'S', 'O', 'N'};
		
		//Print out the arrays
		myArray array = new myArray();
		System.out.println("Array intArray contains: ");
		array.printArray(intArray);
		System.out.println("Array doubleArray contains: ");
		array.printArray(doubleArray);
		System.out.println("Array charArray contains: ");
		array.printArray(charArray);
	}

}
