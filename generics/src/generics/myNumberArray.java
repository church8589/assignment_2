package generics;



public class myNumberArray {
	
	
	public <E extends Number> void printArray(E[] inputArray) {
		for(E element : inputArray) {
			System.out.printf("%s\n", element);
		}
	}

	public static void main(String[] args) {
		// Create Arrays of Integer, double and Character
		Integer[] intArray = {1,2,3,4,5,};
		Double[] doubleArray = {1.1,2.2,3.3,4.4};
		Long[] longArray = {15468438L, 64865435L, 6849L, 1684635L};
		
		//Print out the arrays
		myNumberArray array = new myNumberArray();
		System.out.println("Array intArray contains: ");
		array.printArray(intArray);
		System.out.println("Array doubleArray contains: ");
		array.printArray(doubleArray);
		System.out.println("Array longArray contains: ");
		array.printArray(longArray);
	}

}
